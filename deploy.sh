source .env

#delete existing containers
sh delete.sh

echo "Docker compose UP..."
compose=$(sh getdockercompose.sh) 2>/dev/null
if [ $? -ne 0 ]
then
    echo "failed with $compose"
    exit $?;
fi
docker-compose --file $compose up -d

sh logs.sh app
