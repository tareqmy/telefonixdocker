source .env

echo "pulling images from docker hub"

docker pull tareqmy/postgres:latest
docker pull tareqmy/telefonixlb:latest
docker pull tareqmy/telefonixsip:latest
docker pull tareqmy/obelix:latest
docker pull tareqmy/getafix:latest
docker pull tareqmy/cacofonix:latest