source .env

if [ $# -eq 1 ]
then

    if [ "$1" == "db" ]; then
        echo "log for $DATABASE"
        docker logs -f $DATABASE
        exit 0;
    elif [ "$1" == "lb" ]; then
        echo "log for $LIQUIBASE"
        docker logs -f $LIQUIBASE
        exit 0;
    elif [ "$1" == "fl" ]; then
        echo "log for $OBELIX"
        docker logs -f $OBELIX
        exit 0;
    elif [ "$1" == "ms" ]; then
        echo "log for $MAILSERVER"
        docker logs -f $MAILSERVER
        exit 0;
    elif [ "$1" == "app" ]; then
        echo "log for $GETAFIX"
        docker logs -f $GETAFIX
        exit 0;
    elif [ "$1" == "ui" ]; then
        echo "log for $CACOFONIX"
        docker logs -f $CACOFONIX
        exit 0;
    fi
fi
echo "log for $ASTERISK"
docker logs -f $ASTERISK
