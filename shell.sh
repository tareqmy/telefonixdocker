source .env

if [ $# -eq 1 ]
then

    if [ "$1" == "db" ]; then
        echo "shell for $DATABASE"
        docker exec -it $DATABASE /bin/bash
        exit 0;
    elif [ "$1" == "lb" ]; then
        echo "shell for $LIQUIBASE"
        docker exec -it $LIQUIBASE /bin/bash
        exit 0;
    elif [ "$1" == "fl" ]; then
        echo "shell for $OBELIX"
        docker exec -it $OBELIX /bin/bash
        exit 0;
    elif [ "$1" == "ms" ]; then
        echo "shell for $MAILSERVER"
        docker exec -it $MAILSERVER /bin/bash
        exit 0;
    elif [ "$1" == "app" ]; then
        echo "log for $GETAFIX"
        docker exec -it $GETAFIX /bin/bash
        exit 0;
    elif [ "$1" == "ui" ]; then
        echo "log for $CACOFONIX"
        docker exec -it $CACOFONIX /bin/bash
        exit 0;
    fi
fi
echo "shell for $ASTERISK"
docker exec -it $ASTERISK /bin/bash
